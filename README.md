# Setra Client

Client for doing HTTP requests to the SETRA api

```python
from setra_client import SetraClient
from setra_client.models import Batch, Transaction, Voucher

client = SetraClient(url='https://example.com',
                headers={'X-Gravitee-API-Key': 'c-d-a-b'})


transactions = [Transaction.from_dict({
    "voucherid": 1,
    "account": 1,
    "amount": 1,
    "transdate": 1,
    "curamount": 1,
    "currency": 1,
    "description": 1,
    "dim1": 1,
    "dim2": 1,
    "dim3": 1,
    "dim4": 1,
    "dim5": 1,
    "dim6": 1,
    "dim7": 1,
    "sequenceno": 1,
    "taxcode": 1,
    "transtype": 1,
    "extinvref": 1
})]

vouchers = [Voucher.from_dict({
    "exref": 3,
    "voucherno": 4,
    "transactions": transactions
})]

batch = Batch.from_dict({
    "client": 1,
    "batchid": 2,
    "period": 3,
    "interface": 4,
    "vouchertype": 5,
    "vouchers": vouchers
})

response = client.post_new_batch(batch)
```

Running tests
-------------


```
pytest
```
or
```
python3 setup.py test
```
