"""Client for connecting to SETRA API"""

import logging
import urllib.parse
from datetime import date
from types import ModuleType
from typing import Any, Dict, List, Literal, Optional, overload, Tuple, Type, Union

import requests

from setra_client.models import (
    BatchErrors,
    BatchProgressEnum,
    CompleteBatch,
    InputBatch,
    OutputBatch,
    Parameter,
    Order,
    Detail,
    AbwOrder,
    AbwOrderErrors,
    ResponseStatusEnum,
    Voucher,
    T,
)

JsonType = Any

logger = logging.getLogger(__name__)


def merge_dicts(*dicts: Optional[Dict[Any, Any]]) -> Dict[Any, Any]:
    """
    Combine a series of dicts without mutating any of them.

    >>> merge_dicts({'a': 1}, {'b': 2})
    {'a': 1, 'b': 2}
    >>> merge_dicts({'a': 1}, {'a': 2})
    {'a': 2}
    >>> merge_dicts(None, None, None)
    {}
    """
    combined = dict()
    for d in dicts:
        if not d:
            continue
        for k in d:
            combined[k] = d[k]
    return combined


class IncorrectPathError(Exception):
    pass


class SetraEndpoints:
    def __init__(
        self,
        url: str,
        batch_url: str = "api/batch/",
        transaction_url: str = "api/transaction/",
        voucher_url: str = "api/voucher/",
        new_batch_url: str = "api/addtrans/",
        put_batch_url: str = "api/addtrans/",
        batch_complete_url: str = "api/batch_complete/",
        batch_error_url: str = "api/batch_error/",
        parameters_url: str = "api/parameters/",
        # Order urls (sotra):
        order_url: str = "api/order/",
        order_complete_url: str = "api/order_complete/",
        detail_url: str = "api/detail/",
        details_in_order_url: str = "api/details_in_order/",
        abw_order_complete_url: str = "api/abw_order_complete/",
        post_add_abw_order_url: str = "api/add_abw_order/",
        abw_order_errors_url: str = "api/abw_order_errors/",
        last_fs_batch_url: str = "api/batch_fs_last/",
    ) -> None:
        self.baseurl = url
        self.batch_url = batch_url
        self.transaction_url = transaction_url
        self.voucher_url = voucher_url
        self.new_batch_url = new_batch_url
        self.put_batch_url = put_batch_url
        self.batch_complete_url = batch_complete_url
        self.batch_error_url = batch_error_url
        self.parameters_url = parameters_url
        # order urls:
        self.order_url = order_url
        self.order_complete_url = order_complete_url
        self.detail_url = detail_url
        self.details_in_order_url = details_in_order_url
        self.abw_order_complete_url = abw_order_complete_url
        self.post_add_abw_order_url = post_add_abw_order_url
        self.abw_order_errors_url = abw_order_errors_url
        self.last_fs_batch_url = last_fs_batch_url

    """ Get endpoints relative to the SETRA API URL. """

    def __repr__(self) -> str:
        return "{cls.__name__}({url!r})".format(cls=type(self), url=self.baseurl)

    def batch(self, batch_id: Optional[str] = None) -> str:
        """
        URL for Batch endpoint
        """
        if batch_id is None:
            return urllib.parse.urljoin(self.baseurl, self.batch_url)
        else:
            return urllib.parse.urljoin(
                self.baseurl, "/".join((self.batch_url, batch_id))
            )

    def transaction(self, trans_id: Optional[str] = None) -> str:
        """
        Url for Transaction endpoint
        """
        if trans_id is None:
            return urllib.parse.urljoin(self.baseurl, self.transaction_url)
        else:
            return urllib.parse.urljoin(
                self.baseurl, "/".join((self.transaction_url, trans_id))
            )

    def voucher(self, vouch_id: Optional[str] = None) -> str:
        """
        Url for Voucher endpoint
        """
        if vouch_id is None:
            return urllib.parse.urljoin(self.baseurl, self.voucher_url)
        else:
            return urllib.parse.urljoin(
                self.baseurl, "/".join((self.voucher_url, vouch_id))
            )

    def post_new_batch(self) -> str:
        return urllib.parse.urljoin(self.baseurl, self.new_batch_url)

    def put_update_batch(self) -> str:
        return urllib.parse.urljoin(self.baseurl, self.new_batch_url)

    def batch_complete(self, batch_id: str) -> str:
        """
        URL for Batch endpoint
        """
        return urllib.parse.urljoin(
            self.baseurl, "/".join((self.batch_complete_url, batch_id))
        )

    def batch_error(self, batch_id: str) -> str:
        """
        URL for batch_error endpoint
        """
        return urllib.parse.urljoin(
            self.baseurl, "/".join((self.batch_error_url, batch_id))
        )

    def parameters(self) -> str:
        """Get url for parameters endpoint"""
        return urllib.parse.urljoin(self.baseurl, self.parameters_url)

    def abw_order_complete(self, abw_order_id: Optional[str] = None) -> str:
        """
        URL for getting an abw order including a list of its orders, and each order contains a list of its detail objects
        """
        if abw_order_id is None:
            raise ValueError(f"Illegal value for abw_order_id: {abw_order_id}")
        return urllib.parse.urljoin(
            self.baseurl, "/".join((self.abw_order_complete_url, abw_order_id))
        )

    def add_abw_order(self) -> str:
        """
        URL for posting a new complete abw order containing order and detail objects
        """
        return urllib.parse.urljoin(self.baseurl, self.post_add_abw_order_url)

    def order(self, order_id: Optional[str] = None) -> str:
        """
        URL for order endpoint
        """
        if order_id is None:
            return urllib.parse.urljoin(self.baseurl, self.order_url)
        else:
            return urllib.parse.urljoin(
                self.baseurl, "/".join((self.order_url, order_id))
            )

    def order_complete(self, order_id: str) -> str:
        """
        URL for getting an order object including a list of its detail objects
        """
        return urllib.parse.urljoin(
            self.baseurl, "/".join((self.order_complete_url, order_id))
        )

    def detail(self, detail_id: str) -> str:
        """
        URL for detail endpoint
        """
        return urllib.parse.urljoin(
            self.baseurl, "/".join((self.detail_url, detail_id))
        )

    def details_in_order(self, order_id: str) -> str:
        """
        URL for getting a list of detail objects in an order
        """
        return urllib.parse.urljoin(
            self.baseurl, "/".join((self.details_in_order_url, order_id))
        )

    def abw_order_errors(self, abw_order_id: str) -> str:
        """
        URL for getting an object containing lists of all errors for AbwOrder, Orders and Details
        """
        return urllib.parse.urljoin(
            self.baseurl, "/".join((self.abw_order_errors_url, abw_order_id))
        )

    def last_fs_batch(self, interface: str) -> str:
        return urllib.parse.urljoin(
            self.baseurl, "/".join((self.last_fs_batch_url, interface))
        )


class SetraClient(object):
    default_headers = {
        "Accept": "application/json",
    }

    def __init__(
        self,
        url: str,
        headers: Optional[Dict[str, str]] = None,
        return_objects: bool = True,
        use_sessions: bool = True,
    ):
        """
        SETRA API client.

        :param str url: Base API URL
        :param dict headers: Append extra headers to all requests
        :param bool return_objects: Return objects instead of raw JSON
        :param bool use_sessions: Keep HTTP connections alive (default True)
        """

        self.urls = SetraEndpoints(url)
        self.headers = merge_dicts(self.default_headers, headers)
        self.return_objects = return_objects
        self.session: Union[ModuleType, requests.Session]
        if use_sessions:
            self.session = requests.Session()
        else:
            self.session = requests

    def _build_request_headers(
        self, headers: Optional[Dict[str, Any]]
    ) -> Dict[str, Any]:
        request_headers: Dict[str, Any] = dict()
        for h in self.headers:
            request_headers[h] = self.headers[h]
        if headers is not None:
            for h in headers:
                request_headers[h] = headers[h]
        return request_headers

    @overload
    def call(
        self,
        method_name: Literal["GET"],
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Optional[Dict[str, Any]] = None,
        return_response: Optional[bool] = True,
        **kwargs: Any,
    ) -> Union[JsonType, str, None]: ...

    @overload
    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Optional[Dict[str, Any]] = None,
        return_response: Optional[bool] = True,
        **kwargs: Any,
    ) -> Union[JsonType, str, requests.models.Response, None]: ...

    def call(
        self,
        method_name: str,
        url: str,
        headers: Optional[Dict[str, Any]] = None,
        params: Optional[Dict[str, Any]] = None,
        return_response: Optional[bool] = True,
        **kwargs: Any,
    ) -> Union[JsonType, str, requests.models.Response, None]:
        if method_name == "GET":
            return_response = False
        headers = self._build_request_headers(headers)
        if params is None:
            params = {}
        logger.debug(
            "Calling %s %s with params=%r",
            method_name,
            urllib.parse.urlparse(url).path,
            params,
        )
        r = self.session.request(
            method_name, url, headers=headers, params=params, **kwargs
        )
        if r.status_code in (500, 400, 401):
            logger.warning("Got HTTP %d: %r", r.status_code, r.content)
        elif r.status_code == 404 and method_name == "GET":
            try:
                data = r.json()
                if "detail" in data and "Not found" in data["detail"]:
                    return None
                else:
                    raise IncorrectPathError
            except:
                data = r.text
                if "Not found" in data:
                    return None
                else:
                    raise IncorrectPathError
        if return_response:
            return r
        r.raise_for_status()

        # If we got a valid answer, return Json if there is a json in it
        # Otherwise, return just the text
        try:
            return_data = r.json()
        except:
            return_data = r.text
        return return_data

    def get(self, url: str, **kwargs: Any) -> Union[JsonType, str, None]:
        return self.call("GET", url, **kwargs)

    def put(
        self, url: str, **kwargs: Any
    ) -> Union[JsonType, str, requests.models.Response, None]:
        return self.call("PUT", url, **kwargs)

    def post(
        self, url: str, **kwargs: Any
    ) -> Union[JsonType, str, requests.models.Response, None]:
        return self.call("POST", url, **kwargs)

    def object_or_data(
        self, cls: Type[T], data: Union[Dict[Any, Any], List[Dict[Any, Any]]]
    ) -> Union[Dict[Any, Any], List[Dict[Any, Any]], T, List[T]]:
        """Create list of objects or return data as is"""
        if not self.return_objects:
            return data
        if isinstance(data, dict):
            return cls.from_dict(data)
        if isinstance(data, list):
            return [cls.from_dict(i) for i in data]
        raise TypeError(f"Expected list or dict, got {type(data)}")

    def search_batches(
        self,
        min_created_date: Optional[date] = None,
        max_created_date: Optional[date] = None,
        batch_progress: Optional[BatchProgressEnum] = None,
        interface: Optional[str] = None,
    ) -> Union[JsonType, str, List[OutputBatch], None]:
        """
        Search batches in SETRA.
        Dates (maximal and minimal creation dates) should
        be defined like ISO strings - like "2020-01-01".
        Batch Progress and interface - like strings exactly
        as the values that can be found in the corresponding
        fields of Setra.
        """

        params = {
            "created__gte": min_created_date,
            "created__lte": max_created_date,
            "batch_progress": batch_progress,
            "interface": interface,
        }

        url = self.urls.batch()
        data = self.get(url, params=params)
        if not self.return_objects:
            return data
        if isinstance(data, list):
            return [OutputBatch(**item) for item in data]
        elif isinstance(data, dict):
            return [OutputBatch(**data)]
        return None

    def get_batch(self, batch_id: str) -> Union[JsonType, str, OutputBatch, None]:
        """
        GETs a batch from SETRA.
        """

        url = self.urls.batch(str(batch_id))
        data: Any = self.get(url)
        if data and self.return_objects:
            return OutputBatch(**data)
        else:
            return data

    def get_voucher(
        self, vouch_id: Union[str, int, None] = None
    ) -> Union[JsonType, str, None]:
        """
        GETs one or all vouchers from SETRA
        """
        if vouch_id is not None:
            vouch_id = str(vouch_id)

        url = self.urls.voucher(vouch_id)
        data = self.get(url)
        return data

    def get_transaction(
        self, trans_id: Union[str, int, None] = None
    ) -> Union[JsonType, str, None]:
        """
        GETs one or all transactions from SETRA
        """
        if trans_id is not None:
            trans_id = str(trans_id)

        url = self.urls.transaction(trans_id)
        data = self.get(url)
        return data

    def post_new_batch(
        self, batchdata: InputBatch
    ) -> Tuple[str, Dict[str, Union[int, JsonType, bytes, None]]]:
        """
        POST combination of batch, vouchers and transactions
        """
        url = self.urls.post_new_batch()
        headers = {"Content-Type": "application/json"}
        response = self.post(
            url,
            data=batchdata.json(exclude_unset=True),
            headers=headers,
            return_response=True,
        )
        try:
            content = response.json()
        except ValueError:
            content = response.content

        if response.status_code == 202:
            return ResponseStatusEnum.ACCEPTED, {"code": 202, "content": None}
        elif response.status_code == 409:
            return ResponseStatusEnum.CONFLICT, {"code": 409, "content": content}
        else:
            return ResponseStatusEnum.UNKNOWN, {
                "code": response.status_code,
                "content": content,
            }

    def put_update_batch(
        self, batchdata: InputBatch
    ) -> Tuple[str, Dict[str, Union[int, JsonType, bytes, None]]]:
        """
        PUT updates an existing batch with vouchers and transactions,
        if the batch exists in setra, and has status=created, or validation failed.
        Returns 404 if no batch was found, 204 for successful update, or 409 if batch
        was found, but did not meet the status criteria mentioned above.
        """
        url = self.urls.put_update_batch()
        headers = {"Content-Type": "application/json"}
        response = self.put(
            url,
            data=batchdata.json(exclude_unset=True),
            headers=headers,
            return_response=True,
        )

        try:
            content = response.json()
        except ValueError:
            content = response.content

        if response.status_code == 204:
            return ResponseStatusEnum.ACCEPTED, {"code": 204, "content": None}
        elif response.status_code == 409:
            return ResponseStatusEnum.CONFLICT, {"code": 409, "content": content}
        else:
            return ResponseStatusEnum.UNKNOWN, {
                "code": response.status_code,
                "content": content,
            }

    def get_batch_complete(
        self, batch_id: str
    ) -> Union[
        CompleteBatch, List[CompleteBatch], Dict[Any, Any], List[Dict[Any, Any]]
    ]:
        """
        GETs complete batch (with vouchers and transactions)
        from SETRA
        """
        url = self.urls.batch_complete(batch_id)
        data: Any = self.get(url)
        return self.object_or_data(CompleteBatch, data)

    def get_batch_errors(
        self, batch_id: str
    ) -> Union[BatchErrors, List[BatchErrors], Dict[Any, Any], List[Dict[Any, Any]]]:
        url = self.urls.batch_error(batch_id)
        data: Any = self.get(url)
        return self.object_or_data(BatchErrors, data)

    def get_parameters(
        self, interface: Optional[str] = None
    ) -> Union[Parameter, List[Parameter], Dict[Any, Any], List[Dict[Any, Any]]]:
        """Make a GET request to the parameters endpoint"""
        url = self.urls.parameters()
        queryparams = None
        if interface:
            queryparams = {"interface": interface}
        data: Any = self.get(url, params=queryparams)
        return self.object_or_data(Parameter, data)

    # Order ("Sotra") functions:

    def get_order(self, order_id: str) -> Union[Order, JsonType, str, None]:
        """
        GETs one order object
        """
        url = self.urls.order(str(order_id))
        data: Any = self.get(url)
        if self.return_objects:
            return Order(**data)
        else:
            return data

    def get_detail(self, detail_id: str) -> Union[Detail, JsonType, str, None]:
        """
        GETs one detail object
        """
        url = self.urls.detail(str(detail_id))
        data: Any = self.get(url)
        if self.return_objects:
            return Detail(**data)
        else:
            return data

    def get_order_list(
        self,
    ) -> Union[Order, List[Order], Dict[Any, Any], List[Dict[Any, Any]]]:
        """
        GETs a list of all orders, without detail objects
        """
        url = self.urls.order()
        data: Any = self.get(url)
        return self.object_or_data(Order, data)

    def get_details_in_order(
        self, order_id: str
    ) -> Union[List[Detail], Any, str, None]:
        """
        GETs list of all detail objects belonging to an order
        """
        url = self.urls.details_in_order(str(order_id))
        data = self.get(url)
        if self.return_objects:
            if isinstance(data, list):
                return [Detail(**item) for item in data]
            elif isinstance(data, dict):
                return [Detail(**data)]
        return data

    def get_order_complete(self, order_id: str) -> Union[Order, JsonType, str, None]:
        """
        GETs one order, with all detail objects
        """
        url = self.urls.order_complete(str(order_id))
        data: Any = self.get(url)
        if self.return_objects:
            return Order(**data)
        else:
            return data

    def get_abw_order_complete(
        self, abw_order_id: str
    ) -> Union[AbwOrder, JsonType, str, None]:
        """
        GETs one abworder, with all order and detail objects
        """
        url = self.urls.abw_order_complete(str(abw_order_id))
        data: Any = self.get(url)
        if self.return_objects:
            return AbwOrder(**data)
        else:
            return data

    def post_add_abw_order(
        self, abworder: AbwOrder
    ) -> Tuple[str, Dict[str, Union[int, JsonType, bytes]]]:
        """
        POST one AbwOrder, with its orders and its details.

        Note: There are some fields that cannot be sent in when creating a new AbwOrder:
        AbwOrder.id
        AbwOrder.progress
        AbwOrder.abworder_validated_ok_date
        Order.id
        Order.abw_order_id
        Detail.id
        Detail.order_id

        Returns tuple, with (data, status)
        """
        url = self.urls.add_abw_order()
        headers = {"Content-Type": "application/json"}
        response = self.post(
            url, data=abworder.json(), headers=headers, return_response=True
        )

        try:
            content = response.json()
        except ValueError:
            content = response.content

        if response.status_code == 202:
            return ResponseStatusEnum.ACCEPTED, {"code": 202, "content": content}
        elif response.status_code == 409:
            return ResponseStatusEnum.CONFLICT, {"code": 409, "content": content}
        else:
            return ResponseStatusEnum.UNKNOWN, {
                "code": response.status_code,
                "content": content,
            }

    def get_abw_order_errors(
        self, abw_order_id: str
    ) -> Union[
        AbwOrderErrors, List[AbwOrderErrors], Dict[Any, Any], List[Dict[Any, Any]]
    ]:
        """
        Gets an object containing three lists of all errors, for each of AbwOrder, Orders and Details
        """
        url = self.urls.abw_order_errors(abw_order_id)
        response: Any = self.get(url)
        return self.object_or_data(AbwOrderErrors, response.json())

    def get_last_fs_batch(
        self, interface: str
    ) -> Union[Dict[Any, Any], List[Dict[Any, Any]], OutputBatch, List[OutputBatch]]:
        url = self.urls.last_fs_batch(interface)
        response: Any = self.get(url)
        return self.object_or_data(OutputBatch, response)


def get_client(config_dict: Dict[str, Any]) -> SetraClient:
    """
    Get a SetraClient from configuration.
    """
    return SetraClient(**config_dict)
