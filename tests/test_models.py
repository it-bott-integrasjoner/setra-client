import datetime

import pytest
from pydantic import ValidationError

from setra_client.models import (
    BatchErrors,
    CompleteBatch,
    ErrorBatch,
    Parameter,
    Transaction,
    Voucher,
    InputBatch,
    OutputBatch,
    AbwOrder,
    Order,
    Detail,
)


def test_batch(batch_fixture):
    assert InputBatch(**batch_fixture)


def test_batch2(batch_without_voucher_field):
    assert OutputBatch(**batch_without_voucher_field)


def test_voucher(voucher_fixture):
    assert Voucher(**voucher_fixture)


def test_voucher_has_transactions(voucher_fixture):
    voucher = Voucher(**voucher_fixture)
    assert len(voucher.transactions) == 2
    assert voucher.transactions[0].amount == 1
    assert voucher.transactions[1].amount == 2


def test_transaction(trans_fixture):
    assert Transaction(**trans_fixture)


def test_transaction_fail(trans_fail_fixture):
    # Check missing required field fails
    with pytest.raises(ValidationError):
        Transaction(**trans_fail_fixture)


def test_batch_with_voucher(batch_with_voucher_fixture):
    assert InputBatch(**batch_with_voucher_fixture)


def test_batch_fail(batch_fail_fixture):
    # Using wrong data format, should fail:
    with pytest.raises(ValidationError):
        OutputBatch(**batch_fail_fixture)


def test_error_batch(error_batch):
    eb = ErrorBatch(**error_batch)
    assert eb.id == 1


def test_batch_error(batch_errors):
    be = BatchErrors(**batch_errors)
    assert isinstance(be.batch_errors, list)
    assert isinstance(be.voucher_errors, list)
    assert isinstance(be.transaction_errors, list)


def test_batch_complete(complete_batch):
    bc = CompleteBatch(**complete_batch)
    assert bc.created == datetime.datetime(
        2020, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
    )


def test_params(param_list):
    param = [Parameter(**i) for i in param_list]
    assert param
    assert param[0].created == datetime.datetime(
        2020, 1, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
    )


def test_abw_order(abw_order_fixture):
    assert AbwOrder(**abw_order_fixture)


def test_order(order_fixture):
    assert Order(**order_fixture)


def test_detail(detail_fixture):
    assert Detail(**detail_fixture)


def test_detail_list(detail_list_fixture):
    details = [Detail(**i) for i in detail_list_fixture]
    assert details
    assert details[0].product_specification is 213
    assert details[1].line_total == 123.00
    assert details[1].product_specification is None


def test_order_with_detail(order_with_detail_fixture):
    order = Order(**order_with_detail_fixture)
    assert order.details is not None
    assert len(order.details) == 2
    assert order.details[0].koststed == "123123"
    assert order.details[1].koststed == "444"


def test_complete_abw_order(complete_abw_order_fixture):
    abworder = AbwOrder(**complete_abw_order_fixture)
    assert len(abworder.orders) == 2
    assert abworder.orders[0].details is not None
    assert abworder.orders[1].details is not None
    assert len(abworder.orders[0].details) == 1
    assert len(abworder.orders[1].details) == 1
    assert abworder.orders[0].id == 3
    assert abworder.orders[0].details[0].id == 2
    assert abworder.orders[1].id == 4
    assert abworder.orders[1].details[0].id == 3
