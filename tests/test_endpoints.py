from setra_client.client import SetraEndpoints


def test_init(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.baseurl == baseurl


def test_init_batchurl(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.batch_url == "api/batch/"


# Test urls:
def test_init_batch_with_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.batch(batch_id="5") == baseurl + "/api/batch/5"


def test_init_batch_with_empty_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.batch(batch_id="") == baseurl + "/api/batch/"


def test_init_batch_without_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.batch() == baseurl + "/api/batch/"


def test_init_transaction_with_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.transaction(trans_id="5") == baseurl + "/api/transaction/5"


def test_init_transaction_without_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.transaction() == baseurl + "/api/transaction/"


def test_init_batch_complete_with_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.batch_complete(batch_id="5") == baseurl + "/api/batch_complete/5"


def test_init_batch_error(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.batch_error("123") == baseurl + "/api/batch_error/123"


def test_init_order_complete_with_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.order_complete(order_id="7") == baseurl + "/api/order_complete/7"


def test_init_abw_order_complete_with_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert (
        endpoints.abw_order_complete(abw_order_id="4")
        == baseurl + "/api/abw_order_complete/4"
    )


def test_init_order(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.order() == baseurl + "/api/order/"


def test_init_order_with_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.order(order_id="4") == baseurl + "/api/order/4"


def test_init_detail_with_value(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.detail(detail_id="3") == baseurl + "/api/detail/3"


def test_init_details_in_order(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert (
        endpoints.details_in_order(order_id="9") == baseurl + "/api/details_in_order/9"
    )


def test_init_add_abw_order(baseurl):
    endpoints = SetraEndpoints(baseurl)
    assert endpoints.add_abw_order() == baseurl + "/api/add_abw_order/"
