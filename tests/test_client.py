import datetime
import json
from json.decoder import JSONDecodeError

import pytest
import requests
from requests import HTTPError
from typing import Any, Dict

from setra_client.client import SetraClient
from setra_client.client import SetraEndpoints
from setra_client.client import IncorrectPathError
from setra_client.models import (
    CompleteBatch,
    BatchErrors,
    Parameter,
    InputBatch,
    OutputBatch,
    AbwOrder,
    Order,
    Detail,
)


@pytest.fixture
def header_name():
    return "X-Test"


@pytest.fixture
def client_cls(header_name):
    class TestClient(SetraClient):
        default_headers = {
            header_name: "6a9a32f0-7322-4ef3-bbce-6685a3388e67",
        }

    return TestClient


def test_init_does_not_mutate_arg(client_cls, baseurl):
    headers: Dict[str, Any] = {}
    client = client_cls(baseurl, headers=headers)
    assert headers is not client.headers
    assert not headers


def test_init_applies_default_headers(client_cls, baseurl, header_name):
    headers: Dict[str, Any] = {}
    client = client_cls(baseurl, headers=headers)
    assert header_name in client.headers
    assert client.headers[header_name] == client.default_headers[header_name]


def test_init_modify_defaults(client_cls, baseurl, header_name):
    headers = {header_name: "ede37fdd-a2ae-4a96-9d80-110528425ea6"}
    client = client_cls(baseurl, headers=headers)
    # Check that we respect the headers arg, and don't use default_headers
    assert client.headers[header_name] == headers[header_name]
    # Check that we don't do this by mutating default_headers
    assert client.default_headers[header_name] != headers[header_name]


# Test call method


def test_get_successful_batch_with_json_content(client, requests_mock, baseurl):
    """A working GET call should return HTTP 200, with json content"""

    url = SetraEndpoints(baseurl).batch(batch_id="3")  # https://localhost/api/batch/3
    requests_mock.get(url, json={"foo": "bar"}, status_code=200)
    response = client.call(method_name="GET", url=url)
    assert response == {"foo": "bar"}


def test_get_successful_batch_with_text_content(client, requests_mock, baseurl):
    """A working GET call should return HTTP 200, with json content"""

    url = SetraEndpoints(baseurl).batch(batch_id="3")  # https://localhost/api/batch/3
    requests_mock.get(url, text="some content", status_code=200)

    response = client.call(method_name="GET", url=url)
    assert response == "some content"


def test_get_failing_batch_with_json_content(client, batch_url, requests_mock, baseurl):
    """A failing GET call with 404 should pass through the same response from the client, and return HTTP 404,
    and the request with json content"""
    requests_mock.get(batch_url, json={"a": "b"}, status_code=404)

    with pytest.raises(IncorrectPathError):
        resp = client.call(method_name="GET", url=batch_url)


def test_get_failing_batch_with_text_content(client, batch_url, requests_mock, baseurl):
    """A failing GET call with 404 should pass through the same response from the client, and return HTTP 404,
    and the request with json content"""
    requests_mock.get(batch_url, text="some content", status_code=404)

    with pytest.raises(IncorrectPathError):
        resp = client.call(method_name="GET", url=batch_url)


def test_get_failing_batch_without_return_response_text(
    client, batch_url, requests_mock, baseurl
):
    """A failing GET call with 404 should raise HTTPError from the client,
    and return HTTP 404, and the request with text content"""
    requests_mock.get(batch_url, text="some content", status_code=404)

    with pytest.raises(IncorrectPathError) as err:
        client.call(method_name="GET", url=batch_url, return_response=False)

    assert err.type == IncorrectPathError


def test_get_failing_batch_without_return_response_json(
    client, batch_url, requests_mock, baseurl
):
    """A failing GET call with 404 should raise HTTPError from the client,
    and return HTTP 404, and the request with json content"""
    requests_mock.get(batch_url, json={"a": "b"}, status_code=404)

    with pytest.raises(IncorrectPathError) as err:
        client.call(method_name="GET", url=batch_url, return_response=False)

    assert err.type == IncorrectPathError


def test_get_failing_batch_without_return_response2(
    client, batch_url, requests_mock, baseurl
):
    """A failing GET call with 404 returning text content, with return_response=False
    should raise for HTTPError from the client
    (because we expect json from setra, and it will give error with text content)"""
    requests_mock.get(batch_url, text="some content", status_code=404)

    with pytest.raises(IncorrectPathError) as err:
        client.call(method_name="GET", url=batch_url)

    assert err.type == IncorrectPathError


# Test post method


def test_get_failing_batch_with_http500(client, batch_url, requests_mock, baseurl):
    """A failing POST call with return_response=False and http 500,
    should raise HTTPError from the client,
    and return HTTP 500, and the request with json content"""
    requests_mock.post(batch_url, json={"a": "b"}, status_code=500)

    with pytest.raises(HTTPError) as err:
        client.post(url=batch_url, return_response=False)

    assert err.type == requests.exceptions.HTTPError
    assert err.value.response.status_code == 500
    assert err.value.response.json() == {"a": "b"}


def test_get_failing_batch_with_http5002(client, batch_url, requests_mock, baseurl):
    """A failing POST call with 500 with return_response=True,
    should just return the response, with 500"""
    requests_mock.post(batch_url, json={"a": "b"}, status_code=500)

    response = client.post(url=batch_url)
    assert response.status_code == 500
    assert response.json() == {"a": "b"}


# Test setting headers


def test_header_replacement(client_with_a_header, batch_url, requests_mock, baseurl):
    """Given a SetraClient created with a "global" header,
    making a request with the same header key,
    should replace the "global" header value"""
    requests_mock.get(
        batch_url,
        text="some content",
        status_code=200,
        headers={"content-type": "replaced"},
    )

    data = client_with_a_header.call(
        method_name="GET", url=batch_url, headers={"content-type": "replaced"}
    )
    assert data == "some content"


def test_header_replacement2(client_with_a_header, batch_url, requests_mock, baseurl):
    """Given a SetraClient created with a "global" header,
    making a request with the same header key,
    should replace the "global" header value,
    while new headers should be added to the request"""
    requests_mock.get(
        batch_url,
        text="some content",
        status_code=200,
        headers={"content-type": "replaced", "key": "val"},
    )

    data = client_with_a_header.call(
        method_name="GET", url=batch_url, headers={"content-type": "replaced"}
    )
    assert data == "some content"


# Test get_batches method


def test_successful_get_all_batches(client, requests_mock, baseurl, batch_fixture):
    """A working GET call should return HTTP 200, with json content"""
    url = SetraEndpoints(baseurl).batch()
    requests_mock.get(url, json=[batch_fixture], status_code=200)

    response = client.search_batches()
    assert response == [OutputBatch(**batch_fixture)]


def test_successful_get_batches_filtered_by_status(
    client, requests_mock, baseurl, batch_fixture
):
    """A working GET call should return HTTP 200, with json content"""
    url = SetraEndpoints(baseurl).batch()
    requests_mock.get(url, json=[batch_fixture], status_code=200)

    response = client.search_batches(batch_progress="ubw_import_ok")
    assert response == [OutputBatch(**batch_fixture)]


def test_successfully_getting_single_batch(
    client, requests_mock, baseurl, batch_fixture
):
    """A working GET call should return HTTP 200, with json content"""
    url = SetraEndpoints(baseurl).batch(batch_id="3")
    requests_mock.get(url, json=batch_fixture, status_code=200)

    response = client.get_batch(3)
    assert response == OutputBatch(**batch_fixture)


def test_failing_to_get_all_batches(client, batch_url, requests_mock, baseurl):
    """Searching for batches with no matches returns empty list"""
    requests_mock.get(batch_url, json=[], status_code=200)

    response = client.search_batches()
    assert response == []


# Test get_voucher method


def test_successfully_getting_single_voucher(client, requests_mock, baseurl):
    """A working GET call should return HTTP 200, with json content"""
    url = SetraEndpoints(baseurl).voucher(vouch_id="5")
    requests_mock.get(url, json={"foo": "bar"}, status_code=200)

    response = client.get_voucher(5)
    assert response == {"foo": "bar"}


def test_requesting_single_voucher_with_invalid_voucherid(
    client, requests_mock, baseurl
):
    """Requesting a voucher, with None as voucherid, will request get all vouchers instead"""
    url = SetraEndpoints(baseurl).voucher()
    requests_mock.get(url, json={"foo": "bar"}, status_code=200)

    response = client.get_voucher(None)  # using None as voucherid
    assert response == {"foo": "bar"}


def test_successfully_getting_single_voucher_with_alphanumeric_voucherid(
    client, requests_mock, baseurl
):
    """Requesting a voucher, with alphanumeric voucherid, will work, and not crash

    TODO: investigate if this is intentional behaviour (need spec on voucher id format)

    """
    url = SetraEndpoints(baseurl).voucher("abcd123efg")
    requests_mock.get(url, json={"foo": "bar"}, status_code=200)

    response = client.get_voucher("abcd123efg")  # using alphanum string as voucherid
    assert response == {"foo": "bar"}


# CLARIFY
def test_failing_to_get_all_vouchers(client, requests_mock, baseurl):
    """A failing GET all vouchers call should still return json"""  # REALLY?
    url = SetraEndpoints(baseurl).voucher()
    requests_mock.get(url, json={"detail": "Not found."}, status_code=404)

    response = client.get_voucher()
    assert response == None


# Test get_transaction method


def test_successfully_getting_single_transaction(client, requests_mock, baseurl):
    """A working GET call should return HTTP 200, with json content"""
    url = SetraEndpoints(baseurl).transaction(trans_id="9")
    requests_mock.get(url, json={"foo": "bar"}, status_code=200)

    response = client.get_transaction(9)
    assert response == {"foo": "bar"}


def test_failing_to_get_all_transactions(client, requests_mock, baseurl):
    """A failing GET all vouchers call should still return json"""
    url = SetraEndpoints(baseurl).transaction()
    requests_mock.get(url, json={"detail": "Not found."}, status_code=404)

    response = client.get_transaction()
    assert response == None


# Test post_new_batch method


def test_successfully_post_batch_with_voucher(
    client, batch_with_voucher_fixture, requests_mock, baseurl
):
    """A working GET call should return HTTP 202, with json content"""
    url = SetraEndpoints(baseurl).post_new_batch()
    batch = InputBatch.from_dict(batch_with_voucher_fixture)
    requests_mock.post(
        url,
        json={},
        status_code=202,
        request_headers={"Content-Type": "application/json"},
    )

    state, data = client.post_new_batch(batch)  # we get a response object back
    assert state == "Accepted"
    assert data == {"code": 202, "content": None}


def test_successfully_post_batch_with_voucher_and_response(
    client, batch_with_voucher_fixture, requests_mock, baseurl
):
    """A working POST new batch call with return_response=True,
    should return the response with HTTP 202, with json content"""
    url = SetraEndpoints(baseurl).post_new_batch()
    requests_mock.post(
        url,
        json={},
        status_code=202,
        request_headers={"Content-Type": "application/json"},
    )  # expect json content

    batch = InputBatch.from_dict(batch_with_voucher_fixture)
    state, data = client.post_new_batch(batch)  # we get a response object back
    assert state == "Accepted"
    assert data == {"code": 202, "content": None}


def test_conflicting_post_new_batch(
    client, batch_with_voucher_fixture, requests_mock, baseurl
):
    url = SetraEndpoints(baseurl).post_new_batch()
    requests_mock.post(
        url,
        json={"error": "batch is being processed"},
        status_code=409,
        request_headers={"Content-Type": "application/json"},
    )  # expect json content
    batch = InputBatch.from_dict(batch_with_voucher_fixture)
    state, data = client.post_new_batch(batch)
    assert state == "Conflict"
    assert data == {"code": 409, "content": {"error": "batch is being processed"}}


def test_unknown_post_new_batch_state(
    client, batch_with_voucher_fixture, requests_mock, baseurl
):
    url = SetraEndpoints(baseurl).post_new_batch()
    requests_mock.post(
        url,
        json={"error": "Batch is malformed, no id"},
        status_code=500,
        request_headers={"Content-Type": "application/json"},
    )  # expect json content
    batch = InputBatch.from_dict(batch_with_voucher_fixture)
    state, data = client.post_new_batch(batch)
    assert state == "Unknown"
    assert data == {"code": 500, "content": {"error": "Batch is malformed, no id"}}


def test_successfully_getting_batch_complete(
    client, requests_mock, baseurl, complete_batch
):
    """A working GET call should return HTTP 200, with json content"""
    url = SetraEndpoints(baseurl).batch_complete(batch_id="1")
    requests_mock.get(url, json=complete_batch, status_code=200)

    response = client.get_batch_complete("1")
    assert response == CompleteBatch(**complete_batch)


def test_get_batch_error(baseurl, requests_mock, client, batch_errors):
    url = SetraEndpoints(baseurl).batch_error("5")
    requests_mock.get(url, json=batch_errors, status_code=200)
    response = client.get_batch_errors("5")
    assert response == BatchErrors(**batch_errors)


def test_successfully_getting_batch_complete_objectless(
    client_no_objects, requests_mock, baseurl, complete_batch
):
    """A working GET call should return HTTP 200, with json content"""
    url = SetraEndpoints(baseurl).batch_complete(batch_id="1")
    requests_mock.get(url, json=complete_batch, status_code=200)

    response = client_no_objects.get_batch_complete("1")
    assert isinstance(response, dict)


def test_get_batch_error_objectless(
    client_no_objects, requests_mock, baseurl, batch_errors
):
    url = SetraEndpoints(baseurl).batch_error("5")
    requests_mock.get(url, json=batch_errors, status_code=200)
    response = client_no_objects.get_batch_errors("5")
    assert isinstance(response, dict)


def test_get_params(client_no_objects, requests_mock, param_list):
    """Check uses correct url and gets dicts."""
    requests_mock.get(
        "https://localhost/api/parameters/", json=param_list, status_code=200
    )
    response = client_no_objects.get_parameters()
    assert isinstance(response, list)
    assert isinstance(response[0], dict)
    assert len(response) == 6
    assert response[0]["created"] == "2020-01-01T02:00:00Z"


def test_get_params_interface(client_no_objects, requests_mock, param_list):
    """Check uses correct url and gets dict."""
    requests_mock.get(
        "https://localhost/api/parameters/?interface=HB",
        json=param_list[2:],
        status_code=200,
    )
    response = client_no_objects.get_parameters(interface="HB")
    assert isinstance(response, list)
    assert isinstance(response[0], dict)
    assert len(response) == 4


def test_get_params_object(client, requests_mock, param_list):
    """Check uses correct url and gets objects."""
    requests_mock.get(
        "https://localhost/api/parameters/", json=param_list, status_code=200
    )
    response = client.get_parameters()
    assert isinstance(response, list)
    assert isinstance(response[0], Parameter)
    assert len(response) == 6
    assert response[5].created == datetime.datetime(
        2020, 1, 1, 2, 0, 0, tzinfo=datetime.timezone.utc
    )
    assert response[5].valid_to == datetime.date(2020, 1, 7)


def test_get_params_interface_object(client, requests_mock, param_list):
    """Check uses correct url and gets objects."""
    requests_mock.get(
        "https://localhost/api/parameters/?interface=HB",
        json=param_list[2:],
        status_code=200,
    )
    response = client.get_parameters(interface="HB")
    assert isinstance(response, list)
    assert isinstance(response[0], Parameter)
    assert len(response) == 4


# Sotra tests:


def test_get_abworder(client, requests_mock, baseurl, complete_abw_order_fixture):
    """Should return HTTP 200, with a complete abw order, with orders and detail objects"""
    url = SetraEndpoints(baseurl).abw_order_complete(abw_order_id="2")
    requests_mock.get(url, json=complete_abw_order_fixture, status_code=200)

    response = client.get_abw_order_complete(2)
    assert response == AbwOrder(**complete_abw_order_fixture)
    assert len(response.orders) == 2
    assert len(response.orders[0].details) == 1
    assert len(response.orders[1].details) == 1


def test_get_order(client, requests_mock, baseurl, order_with_detail_fixture):
    """Should return HTTP 200, with a complete order with detail objects"""
    url = SetraEndpoints(baseurl).order_complete(order_id="2")
    requests_mock.get(url, json=order_with_detail_fixture, status_code=200)

    response = client.get_order_complete(2)
    assert response == Order(**order_with_detail_fixture)


def test_get_detail(client, requests_mock, baseurl, detail_fixture):
    """Should return HTTP 200, with one detail"""
    url = SetraEndpoints(baseurl).detail(detail_id="1")
    requests_mock.get(url, json=detail_fixture, status_code=200)

    response = client.get_detail(1)
    assert response == Detail(**detail_fixture)


def test_get_order_list(client, requests_mock, baseurl, order_list_fixture):
    """Should return HTTP 200, with a list of orders"""
    url = SetraEndpoints(baseurl).order()
    requests_mock.get(url, json=order_list_fixture, status_code=200)

    response = client.get_order_list()
    assert isinstance(response, list)
    assert len(response) == 4


def test_get_order_details_list(client, requests_mock, baseurl, detail_list_fixture):
    """Should return HTTP 200, with a list of details for an order"""
    url = SetraEndpoints(baseurl).details_in_order("1")
    requests_mock.get(url, json=detail_list_fixture, status_code=200)

    response = client.get_details_in_order(order_id="1")
    assert isinstance(response, list)
    assert len(response) == 2
    assert response[0].product_specification is 213
    assert response[1].product_specification is None


def test_send_in_abworder(client, requests_mock, baseurl, complete_abw_order_fixture):
    """Setra client should give us a tuple, with the data, and a status"""
    url = SetraEndpoints(baseurl).add_abw_order()
    resp = {
        "responsible": "responsible2",
        "interface": "testinterface",
        "client": "testclient",
    }
    requests_mock.post(url, json=resp, status_code=202)
    abworder = AbwOrder(**complete_abw_order_fixture)

    response = client.post_add_abw_order(abworder)
    if isinstance(response, tuple):
        assert response[0] == "Accepted"
        assert response[1] == {
            "code": 202,
            "content": {
                "responsible": "responsible2",
                "interface": "testinterface",
                "client": "testclient",
            },
        }


def test_send_in_abworder_failure_conflict(
    client, requests_mock, baseurl, complete_abw_order_fixture
):
    """Setra client should give us a tuple, with the data, and a status"""
    url = SetraEndpoints(baseurl).add_abw_order()
    resp = {
        "responsible": "responsible2",
        "interface": "testinterface",
        "client": "testclient",
    }
    requests_mock.post(url, json=resp, status_code=409)
    abworder = AbwOrder(**complete_abw_order_fixture)

    response = client.post_add_abw_order(abworder)
    assert isinstance(response, tuple)
    assert response[1]["content"] == resp
    assert response[0] == "Conflict"


def test_send_in_abworder_failure(
    client, requests_mock, baseurl, complete_abw_order_fixture
):
    """Setra client should get HTTPError if Setra returns 404"""
    url = SetraEndpoints(baseurl).add_abw_order()
    requests_mock.post(url, status_code=404)
    abworder = AbwOrder(**complete_abw_order_fixture)

    response = client.post_add_abw_order(abworder)

    assert isinstance(response, tuple)
    assert response[0] == "Unknown"
    assert response[1]["code"] == 404
