import json
import os

import pytest

from setra_client.client import SetraClient, SetraEndpoints


def load_json_file(name):
    """Load json file from the fixtures directory"""
    here = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__).rsplit("/", 1)[0])
    )
    with open(os.path.join(here, "tests/fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def baseurl():
    return "https://localhost"


@pytest.fixture
def endpoints(baseurl):
    return SetraEndpoints(baseurl)


@pytest.fixture
def custom_endpoints(baseurl):
    return SetraEndpoints(
        baseurl,
        "/custom/batch/",
        "/custom/transaction/",
        "/custom/voucher/",
        "/custom/addtrans/",
        "/custom/batch_complete/",
    )


@pytest.fixture
def client(baseurl):
    return SetraClient(baseurl)


@pytest.fixture
def client_no_objects(baseurl):
    return SetraClient(baseurl, return_objects=False)


@pytest.fixture
def client_with_a_header(baseurl):
    return SetraClient(baseurl, {"content-type": "test"})


@pytest.fixture
def batch_url(baseurl):
    return SetraEndpoints(baseurl).batch()  # example: https://localhost/api/batch


@pytest.fixture
def batch_fixture():
    return load_json_file("batch_fixture.json")


@pytest.fixture
def voucher_fixture():
    return load_json_file("voucher_fixture.json")


@pytest.fixture
def trans_fixture():
    return load_json_file("trans_fixture.json")


@pytest.fixture
def trans_fail_fixture():
    return load_json_file("trans_fail_fixture.json")


@pytest.fixture
def batch_with_voucher_fixture():
    return load_json_file("batch_with_voucher_fixture.json")


@pytest.fixture
def batch_without_voucher_field():
    return load_json_file("batch_without_voucher_field.json")


@pytest.fixture
def batch_fail_fixture():
    return load_json_file("batch_fail_fixture.json")


@pytest.fixture
def error_batch():
    return load_json_file("error_batch.json")


@pytest.fixture
def batch_errors():
    return load_json_file("batch_errors.json")


@pytest.fixture
def complete_batch():
    return load_json_file("complete_batch.json")


@pytest.fixture
def param_list():
    return load_json_file("params.json")


@pytest.fixture
def abw_order_fixture():
    return load_json_file("abw_order_fixture.json")


@pytest.fixture
def order_fixture():
    return load_json_file("order_fixture.json")


@pytest.fixture
def detail_fixture():
    return load_json_file("detail_fixture.json")


@pytest.fixture
def order_with_detail_fixture():
    return load_json_file("order_with_detail_fixture.json")


@pytest.fixture
def order_list_fixture():
    return load_json_file("order_list_fixture.json")


@pytest.fixture
def detail_list_fixture():
    return load_json_file("detail_list_fixture.json")


@pytest.fixture
def complete_abw_order_fixture():
    return load_json_file("complete_abw_order_fixture.json")
